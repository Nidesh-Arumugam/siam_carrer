import { React, useState, useEffect } from "react";
import { Spinner, Button } from "react-bootstrap";
import "../App.css";
import RectHtmlParser from "react-html-parser";

function Carrer() {
  const [data, setData] = useState(null);
  const [careerData, setCarrerData] = useState(null);

  const [hide, setHide] = useState(false);
  const [showDetail, setShowDetail] = useState(false);

  const [isLoading, setIsLoading] = useState(false);

  useEffect(() => {
    setIsLoading(true);
    const fetchData = async () => {
      const response = await fetch(
        `https://intentcompany.backendless.app/api/data/siam_webapp_carreerpage_and_details?property=objectId&property=short_details&sortBy=%60created%60%20desc`
      );
      const newData = await response.json();
      setIsLoading(false);
      console.log("newData", newData);
      setData(newData);
    };

    fetchData();
  }, []);

  const carrerDescr = (objId) => {
    setIsLoading(true);
    const fetchData = async () => {
      const response = await fetch(
        `https://intentcompany.backendless.app/api/data/siam_webapp_carreerpage_and_details/${objId}`
      );
      const newData = await response.json();
      setIsLoading(false);
      console.log("objId", newData);
      setCarrerData(newData);
    };

    window.scrollTo({ top: 0, behavior: "smooth" });
    setShowDetail(true);
    fetchData();
  };

  return (
    <div className="Carrer">
      {isLoading ? (
        <>
          <div className="loadinStyle">
            <Spinner animation="border" variant="danger" />
          </div>{" "}
        </>
      ) : 
        <>
        {showDetail ? (
        <>
          <div className="jobDes">
            <div className="d-flex gap-2">
              <span> &#60; </span>
              <p
                className="mb-0 cursorPointer jobListingTxt f16"
                onClick={() => setShowDetail(false)}
              >
                {" "}
                Back to job Listings{" "}
              </p>
            </div>
            <div className="mt-5">
              <h5 className="jobTitle"> {careerData?.career_details.title} </h5>
              <p className="mb-0 f18 jobLocation pt-2">
                {" "}
                Location: {careerData?.short_details.location} / Remote
              </p>
              <div className="mt-5">
                {RectHtmlParser(careerData?.career_details.content)}
              </div>
              <Button variant="dark" className="mt-4 applyButton mb-3">
                {" "}
                Apply to this job{" "}
              </Button>
            </div>
          </div>
        </>
      ) : (
        <>
          <div className="d-flex justify-content-center p-5">
            <h3 className="currentOpeningTxt"> Current Openings </h3>
          </div>

          <div className="carrerTabHeight">
            <div
              className={`overallCarrerTabs ${
                hide ? "showCareer" : "hideCareer"
              }`}
            >
              {data?.map((item, index) => (
                <>
                  <div
                    className="subCareerTab w-100 cursorPointer"
                    onClick={() => carrerDescr(item.objectId)}
                  >
                    <p className="mb-0 carrerTitle">
                      {" "}
                      {item.short_details.title}{" "}
                    </p>
                    <div className="careerInfo">
                      <p className="mb-0 location f20">
                        {" "}
                        {item.short_details.location}{" "}
                      </p>
                      <p className="mb-0 f14 ps-3">
                        {" "}
                        Posted: {item.short_details.posted_at}{" "}
                      </p>
                    </div>
                  </div>
                </>
              ))}
              {/* <div className="bg-primary">
          <Spinner animation="border" variant="danger" />
          </div> */}
            </div>

            <div
              className="loadMore f18 cursorPointer"
              onClick={() => setHide(!hide)}
            >
              {hide ? "Hide" : "Load More..."}
            </div>
          </div>
        </>
      )} 
        </>
      }
    </div>
  );
}

export default Carrer;
