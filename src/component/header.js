import { React, useState, useEffect } from "react";
import "../App.css";
import logo from "../assets/siam-computing-trans.png";
import { Button, Dropdown } from "react-bootstrap";

function Header() {
  const [addToCart, setAddToCart] = useState(false);
  const [selectedItemIndex, setSelectedItemIndex] = useState(-1);
  const [scrollChange,setScrollChange] = useState();

  const [state, setState] = useState({
    showDropdown: false,
  });

  const cursorEnter = (id) => {
    setState({ [id]: true });
  };
  const cursorLeave = (id) => {
    setState({ [id]: false });
  };

  const changeLogo = () => {
    if (window.scrollY >= 10) {
      setScrollChange(true);
    } else {
      setScrollChange(false);
    }
  };

  useEffect(() => {
    changeLogo();
    window.addEventListener("scroll", changeLogo);
  });


  const navlist = [
    {
      id: 1,
      title: "Service",
      child: true,
      children: [
        {
          id: 1.1,
          title: "Product strategy and consulting",
        },
        {
          id: 1.2,
          title: "MVP Development",
        },
        {
          id: 1.2,
          title: "Dedicated Product Team",
        },
      ],
    },
    {
      id: 2,
      title: "Skills",
      child: true,
      children: [
        {
          id: 2.1,
          title: "Mobile App Development",
        },
        {
          id: 2.2,
          title: "Hybrid App Development",
        },
        {
          id: 2.3,
          title: "Progressive Web App Development",
        },
      ],
    },
    {
      id: 3,
      title: "About",
      child: true,
      children: [
        {
          id: 3.1,
          title: "Our Journey",
        },
        {
          id: 3.2,
          title: "Life at  Siam",
        },
        {
          id: 3.3,
          title: "Careers",
        },
      ],
    },
    {
      id: 4,
      title: "Resoures",
      child: true,
      children: [
        {
          id: 4.1,
          title: "Blogs",
        },
        {
          id: 4.2,
          title: "Case Studies",
        },
        {
          id: 4.3,
          title: "Guides",
        },
      ],
    },
  ];

  return (
    <div className={`header ${scrollChange && "headerBoxshowdow"}`}>
      <div className="logoStyle">
        <img src={logo} alt="LOGO" className="logoImg" />
      </div>

      <div className="d-flex dropdownList">
        {navlist.map(
          (item, index) => (
            <Dropdown onMouseOver={() => cursorEnter("showDropdown" + item.id)}
            onMouseLeave={() => cursorLeave("showDropdown" + item.id)}
            show={state["showDropdown" + item.id]} className = "toggleDropdown">
              <Dropdown.Toggle
                variant="success"
                id="dropdown-basic"
                className="navBarList"
              >
                {item.title}
              </Dropdown.Toggle>

              <Dropdown.Menu
                id={item.id}
              >
                {item.children.map((subItem, index) => (
                  <Dropdown.Item href="#/action-1">
                    {" "}
                    {subItem.title}{" "}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          )

          
        )}
        <Button variant="outline-danger" className="freeCallButton"> Book a free call </Button>{" "}
      </div>
    </div>
  );
}

export default Header;
