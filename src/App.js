import React from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import Header from './component/header';
import Footer from './component/footer';
import Carrer from './pages/carrer';


function App() {
  
  return (
    <div className="App">
      <Header/>
      <Carrer/>
      <Footer/>
    </div>
  );
}

export default App;
